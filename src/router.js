import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";



Vue.use(Router);

export default new Router ({
    mode:"history",
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home
        },
        {
            path: "/about/:id",
            name: "About",
            component: ()=>
            import (/*webpackChunkName: "about"*/  "./views/About.vue")
        },
        {
            path: "/info/",
            name: "Info",
            component: ()=>
            import (/*webpackChunkName: "Info"*/  "./components/InfoUser.vue")

        },

    ]

});